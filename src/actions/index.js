import * as types from '../constants/ActionTypes';

import {
  UserSchema,
  BankArraySchema
} from '../schemas';

export const authLogin = (login) => {
  login.grant_type = 'password';
  login.scope = 'eoffice offline_access';

  return {
    types: [ types.API_REQUEST_SEND, types.AUTH_SET_TOKEN, types.AUTH_DISCARD_TOKEN ],
    payload: {
      client: 'auth',
      request:{
        url: '/connect/token',
        method: 'POST',
        headers: {
          'Authorization': 'Basic ZW9mZmljZS5yby5jbGllbnQ6dGN4LnNlY3JldA=='
        },
        data: login
      }
    }
  };
};

export const authLogout = () => ({ type: types.AUTH_DISCARD_TOKEN });

export const setTokenWithQuery = (token) => ({
  type: types.AUTH_SET_TOKEN,
  payload: {
    data: token
  }
});

export const userAuthenticated = (roleId = null) => (dispatch, getState) => dispatch({
  types: [ types.API_REQUEST_SEND, types.AUTH_SET_USER, types.AUTH_DISCARD_TOKEN ],
  schema: UserSchema,
  payload: {
    client: 'auth',
    request:{
      url: '/api/me',
      method: 'GET',
      headers: {
        'Authorization': getState().auth.token.tokenType + ' ' + getState().auth.token.accessToken
      }
    }
  },
  roleId
});

export const setPrice = (price) => ({ type: types.SET_PRICE, price });

export const getBanks = () => (dispatch, getState) => dispatch({
  types: [ types.API_REQUEST_SEND, types.GET_BANKS, types.API_REQUEST_ERROR ],
  schema: BankArraySchema,
  payload: {
    request:{
      url: '/banksData.json',
      method: 'GET'
    }
  }
});

export const checkValidBankNumber = (number) => (dispatch, getState) => dispatch({
  types: [ types.API_REQUEST_SEND, types.CHECK_BANK_NUMBER_SUCCESS, types.CHECK_BANK_NUMBER_ERROR ],
  payload: {
    client: 'mock',
    request:{
      url: `/banks/number/${number}`,
      method: 'GET'
    }
  }
});

function action(type, payload = {}) {
  return {type, ...payload}
}

export const loadBanks = () => action(types.GET_BANKS);
export const checkValidateBank = (number) => action(types.CHECK_BANK_NUMBER, {number});