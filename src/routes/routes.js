import {
  HomePage,
  // Login,
  // ErrorPage
} from '../containers/pages';

import App from '../containers/App';

const routes = [
  {
    component: App,
    // requireLogin: '/dang-nhap',
    routes: [
      {
        path: '/',
        exact: true,
        component: HomePage
      },
      // {
      //   component: Login,
      //   path: '/dang-nhap'
      // },
      // {
      //   component: ErrorPage,
      //   path: '/error-page'
      // }
    ]
  }
];

export default routes;
