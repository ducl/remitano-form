import 'babel-polyfill';
import 'bootstrap/dist/css/bootstrap.css'
import React from 'react';
import ReactDOM from 'react-dom';
import { unregister } from './registerServiceWorker';
import configureStore from './store';
import { createBrowserHistory } from 'history';
import { syncHistoryWithStore } from 'react-router-redux';
import { Root } from './containers';
import { routes } from './routes';
import mySaga from './sagas'

const store = configureStore();
const browserHistory = createBrowserHistory();
const history = syncHistoryWithStore(browserHistory, store);
store.runSaga(mySaga);

ReactDOM.render(
  <Root store={store} routes={routes} history={history}/>
, document.getElementById('root'));
unregister();
