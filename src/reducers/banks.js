import {
  GET_BANKS_SUCCESS,
  CHECK_BANK_NUMBER_SUCCESS,
  CHECK_BANK_NUMBER_ERROR,
  CHECK_BANK_NUMBER_SEND
} from '../constants/ActionTypes';

const initialState = {
  error: null,
  name: '',
  items: []
};

export default function settings(state = initialState, action) {
  switch (action.type) {
    case GET_BANKS_SUCCESS:
      return {
        ...state,
        items: action.payload.data.result.result
      };
    case CHECK_BANK_NUMBER_SEND:
      return {
        ...state,
        error: null,
      };
    case CHECK_BANK_NUMBER_SUCCESS:
      return {
        ...state,
        error: null,
        name: action.payload.data.name
      };
    case CHECK_BANK_NUMBER_ERROR:
      return {
        ...state,
        error: 'Invalid account.',
        name: ''
      };
    default:
      return state;
  }
};
