import auth from './auth';
import entities from './entities';
import settings from './settings';
import banks from './banks';
import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

const appReducer = combineReducers({
  auth,
  entities,
  settings,
  banks,
  routing: routerReducer
});

const rootReducer = (state, action) => {
  return appReducer(state, action);
}

export default rootReducer;
