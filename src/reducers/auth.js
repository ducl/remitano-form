import { Cookies } from 'react-cookie';
import {
  AUTH_SET_TOKEN,
  AUTH_DISCARD_TOKEN,
  AUTH_SET_USER
} from '../constants/ActionTypes';

const cookies = new Cookies();
let initialState = cookies.get('authState') || {};

export default function auth(state = initialState, action) {
  let authState = state;

  switch (action.type) {
    case AUTH_SET_TOKEN:
      authState = {
        ...state,
        token: action.payload.data
      };
      break;

    case AUTH_DISCARD_TOKEN:
      return authState = {};

    case AUTH_SET_USER:
      const user = action.payload.data.entities.users[action.payload.data.result.result];

      authState = {
        ...state,
        user
      };
      break;

    default:
      authState = state;
  }

  cookies.set('authState', authState, {path: '/'});

  return authState;
};
