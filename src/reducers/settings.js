import {
  SET_PRICE
} from '../constants/ActionTypes';

const initialState = {
  price: 0
};

export default function settings(state = initialState, action) {
  switch (action.type) {
    case SET_PRICE:
      return {
        ...state,
        price: action.price
      };
    default:
      return state;
  }
};
