import React, { Component } from 'react';
import classnames from 'classnames';
import { MasterLayout } from '../../components/layouts';
import { connect } from 'react-redux';
// import { bindActionCreators } from 'redux';
import { DebounceInput } from 'react-debounce-input';
import * as Actions from '../../actions';
import './assets/home.css';

class HomePage extends Component {
  state = {
    isChangePrice: false,
    isChangeMethod: false,
    bankNumber: '',
    bankAccountName: '',
    error: null
  }

  componentDidMount() {
    this.props.setPrice(Math.floor(Math.random() * 99999) + 10000);
    setInterval(() => this.props.setPrice(Math.floor(Math.random() * 99999) + 10000), 5000);

    this.props.loadBanks();
  }

  toggleChangePrice = (e) => {
    e.preventDefault();

    this.setState({
      isChangePrice: !this.state.isChangePrice
    });
  }

  toggleChangeMethod = (e) => {
    e.preventDefault();

    this.setState({
      isChangeMethod: !this.state.isChangeMethod
    });
  }

  handleChangeBankNumber = async(e) => {
    const { value } = e.target;

    let error = null;

    if (value.length < 13) {
      error = 'Invalid account.'
    } else {
      this.props.checkValidBankNumber(value);
    }

    this.setState({
      bankNumber: value,
      error
    });
  }

  handleRetry = async(e) => {
    e.preventDefault();
    this.setState({
      error: null
    }, () => this.props.checkValidBankNumber(this.state.bankNumber));
  }

  handleSubmit(e) {
    e.preventDefault();
    console.log('Ok.');
  }

  render() {
    const { isChangePrice, isChangeMethod, error, bankNumber } = this.state;
    const banks = this.props.banks.items;
    const bankAccountName = this.props.banks.name;
    const checkError = this.props.banks.error;

    return (
      <MasterLayout>
        <div className="container">
          <div className="row">
            <div className="col-md-12 text-align">
              <div className="panel panel-default">
                <div className="panel-heading black-header">
                  <div className="text-center">
                    Biểu mẫu tạo quảng cáo
                  </div>
                </div>
                <div className="panel-body">
                  <div className="row">
                    <div className="col-md-12">
                      <h3>THÔNG TIN QUẢNG CÁO</h3>
                      <div className="form-group">
                        <label htmlFor="type">Tên ngân hàng</label>
                        <select className="form-control" id="type">
                          <option>Bán Bitcoin</option>
                          <option>Mua Bitcoin</option>
                        </select>
                      </div>
                    </div>
                    <div className="col-md-8">
                      {
                        isChangePrice &&
                        <div className="row">
                          <div className="col-md-12">
                            <div className="form-group">
                              <label htmlFor="currency">Giá</label>
                              <div className="input-group">
                                <input type="text" name="currency" className="form-control" id="currency" aria-describedby="basic-addon2" />
                                <span className="input-group-addon" id="basic-addon2">&nbsp;&nbsp;VND&nbsp;&nbsp;</span>
                              </div>
                            </div>
                          </div>
                        </div>
                      }
                      <div className="row">
                        <div className="col-md-5">
                          <p>Giá Bitcoin bạn thực hiện</p>
                        </div>
                        <div className="col-md-5">
                          <p><b>399,545,965 VND / BTC</b></p>
                        </div>
                        <div className="col-md-2">
                          {
                            !isChangePrice &&
                            <a href="/#" className="pink-color" onClick={this.toggleChangePrice}>Thay đổi</a>
                          }
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-5">
                          <p>Giá bitUSD</p>
                        </div>
                        <div className="col-md-5">
                          <p><b>22,750.0 VND / bitUSD</b></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-4">
                      <div className="row">
                        <p className="gray-color"><i>Giá 1 bitUSD theo VND</i></p>
                      </div>
                      <div className="row">
                        <p className="gray-color"><i>Đây là giá tương ứng với tỉ giá Bitstamp (Blockchain) hiện tại (<b className="pink-color">{this.props.price}</b>). Giá sẽ biến động khi tỉ giá Bitstamp (Blockchain) thay đổi.</i></p>
                      </div>
                    </div>
                  </div>
                  <hr />
                  <div className="row">
                    <form onSubmit={this.handleSubmit}>
                      <div className="col-md-12">
                        <h3>THÔNG TIN THANH TOÁN</h3>
                      </div>
                      <div className="col-md-8">
                        {
                          !isChangeMethod &&
                          <div className="row">
                            <div className="col-md-5">
                              <p>Phương thức thanh toán</p>
                            </div>
                            <div className="col-md-5">
                              <p><b>Chuyển khoản ngân hàng</b></p>
                            </div>
                            <div className="col-md-2">
                              <a href="/#" className="pink-color" onClick={this.toggleChangeMethod}>Thay đổi</a>
                            </div>
                          </div>
                        }
                        <div className="row">
                          <div className="col-md-5">
                            <p>Giá bitUSD</p>
                          </div>
                          <div className="col-md-5">
                            <p><b>22,750.0 VND / bitUSD</b></p>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-4">
                        <div className="row">
                          <p className="gray-color"><i>Phương thức thanh toán cho quảng cáo này</i></p>
                        </div>
                      </div>
                      <div className="col-md-8">
                        {
                          isChangeMethod &&
                          <div className="form-group">
                            <label htmlFor="send">Phương thức thanh toán</label>
                            <select className="form-control" id="send">
                              <option>Chuyển khoản ngân hàngg</option>
                              <option>Bán Bitcoin</option>
                              <option>Mua Bitcoin</option>
                            </select>
                          </div>
                        }
                        <div className="form-group">
                          <label htmlFor="bank">Tên ngân hàng</label>
                          <select className="form-control" id="bank">
                            <option>Chọn ngân hàng</option>
                            {
                              banks.map((item, k) => <option key={k} value={item.id}>{item.name}</option>)
                            }
                          </select>
                        </div>
                        <div className={classnames('form-group', {
                          'has-error': error || checkError
                        })}>
                          <label htmlFor="number">Số tài khoản</label>
                          <DebounceInput
                            type="number"
                            className="form-control"
                            id="number"
                            name="number"
                            minLength={0}
                            debounceTimeout={300}
                            value={bankNumber}
                            onChange={this.handleChangeBankNumber}
                          />
                          {
                            (error || checkError) &&
                            <span className="help-block">
                              {error || checkError}
                              {
                                bankNumber.length >= 13 &&
                                <a href="/#" onClick={this.handleRetry}>( Retry! ) </a>
                              }
                            </span>
                          }
                        </div>
                        <div className="form-group">
                          <label htmlFor="name">Tên tài khoản</label>
                          <input
                            type="input"
                            className="form-control"
                            id="name"
                            name="name"
                            value={bankAccountName}
                            disabled={bankAccountName !== ''}
                          />
                        </div>
                        <button
                          type="submit"
                          className="btn btn-default submit-button"
                          disabled={bankNumber.length < 13 || error || checkError}
                        >Tạo</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </MasterLayout>
    );
  }
}

const mapStateToProps = state => ({
  price: state.settings.price,
  banks: {
    ...state.banks,
    error: state.banks.error,
    items: state.banks.items.map(id => state.entities.banks[id])
  }
});

// const mapDispatchToProps = dispatch => ({
//   actions: bindActionCreators(Actions, dispatch)
// });

export default connect(
  mapStateToProps,
  {
    loadBanks: Actions.loadBanks,
    setPrice: Actions.setPrice,
    checkValidBankNumber: Actions.checkValidateBank
  },
)(HomePage);

