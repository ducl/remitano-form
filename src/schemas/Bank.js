import { schema } from 'normalizr';

const Bank = new schema.Entity('banks', {}, {
  idAttribute: bank => bank.id
});

export default Bank;
