import { put, call, takeLatest, take, fork, all } from 'redux-saga/effects'
import * as types from '../constants/ActionTypes';
import {
  BankArraySchema
} from '../schemas';

function* fetchBanks(action) {
  yield call(getBanks);
}

function* getBanks(params) {
  yield put({
    types: [ types.API_REQUEST_SEND, types.GET_BANKS_SUCCESS, types.API_REQUEST_ERROR ],
    schema: BankArraySchema,
    payload: {
      request:{
        url: '/banksData.json',
        method: 'GET'
      }
    },
    params
  });
}

function* validateBankNumber() {
  while(true) {
    const {number} = yield take(types.CHECK_BANK_NUMBER)

    yield put({
      types: [ types.CHECK_BANK_NUMBER_SEND, types.CHECK_BANK_NUMBER_SUCCESS, types.CHECK_BANK_NUMBER_ERROR ],
      payload: {
        client: 'mock',
        request:{
          url: `/banks/number/${number}`,
          method: 'GET'
        }
      }
    });
  }
}

function* mySaga() {
  yield all([
    takeLatest(types.GET_BANKS, fetchBanks),
    fork(validateBankNumber)
  ])
}

export default mySaga;