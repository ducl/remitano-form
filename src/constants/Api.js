// Default pagination
export const PAGINATION_LIMIT = 10;
export const PAGE = 1;
export const LIMIT_ZERO = 0;

export const AUTH_API_URL = 'http://example.com';
export const API_URL = process.env.PUBLIC_URL;
export const MOCK_API = 'http://demo5876104.mockable.io';
export const UPLOAD_API = 'http://example.com';