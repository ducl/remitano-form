### Remitano create ads form

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app). I also my boilerplate for react app so if can have files unnecessary.

=======
## Install
Install node modules
```sh
npm install
```
Start with development mode
```sh
npm start
```

=======
## Note

I used [Mockable.io](https://mockable.io) for mock validate bank account number. If can't validate with API please contact me for restart it.

Thanks.